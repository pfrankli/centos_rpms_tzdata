#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   tzdata-test.sh of /CoreOS/tzdata/sanity
#   Description: Basic sanity test
#   Original author: Ulrich Drepper <drepper@redhat.com>
#   Modified and broken into separate files by: John Poelstra <poelstra@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2006 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ZONES="timezones.txt"
DATES="dates.txt"
EXPECTED="expected.results"
GENERATED="$(mktemp)"

cat $ZONES | while read timezone; do
    echo "timezone == $timezone" | tee -a $OUTPUTFILE
    cat $DATES | while read datetime; do
        echo "datetime == $datetime" | tee -a $OUTPUTFILE
        printf "%s %s -> %s\n" "$timezone" "$datetime" "$(env TZ=$timezone date -d \
             "$datetime")" | tee -a $OUTPUTFILE | tee -a $GENERATED
    done
done

if diff -u $EXPECTED $GENERATED
then
    echo "Test PASSED" | tee -a $OUTPUTFILE
else
    echo "Test FAILED" | tee -a $OUTPUTFILE
    2>&1 | tee -a $OUTPUTFILE
    exit 1
fi
